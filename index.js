//alert("Hello!")

// Array Methods

/*
1. Mutaror Methods
	Mutator methods are functions that mutate or change an array after they are created. These methods manipulate the original array performing various tasks such as adding and removing elements.

*/

let fruits = ["Apple", "Orange", "Kiwi", "Dragon Fruit"];

/*
	push ()
		Add an element in the end of an array AND returns the array's length

	Syntax:
		arrayName.push(element)

*/

console.log("Current Array:")
console.log(fruits)


let fruitsLength = fruits.push("Mango");
console.log(fruitsLength);
console.log("Mutated array from push method:");
console.log(fruits)

fruits.push("Avocado", "Guava");
console.log(fruits)


/*
	pop()
		Removes the last element in our array AND returns the removed element

		Syntax:
			arrayName.pop()

*/

let removedFruit = fruits.pop()
console.log(removedFruit)
console.log("Mutated array from the pop method")
console.log(fruits)

/*
	unshift()
		Adds one or more elements at the beginning off an array

	Syntax:
		arrayName.unshift("element")

*/

fruits.unshift("Lime", "Banana")
console.log("Mutated array from unshift method:")
console.log(fruits)

/*
	shift()
		Remove an element at the beginning of our array AND returns the removed element

	Syntax:
		arrayName.shift()

*/

let anotherFruit = fruits.shift()
	console.log(anotherFruit);
	console.log("Mutated array from shift method: ");
	console.log(fruits)


/*
	splice()
		Simultaneously removes elements from a specified index number and adds an element

	Syntax:
		arrayName.splice(startingIndex, deleteCount, elementsToBeAdded)

*/

fruits.splice(1,2,"Lime", "Cherry")
console.log("Mutated array from splice method: ")
console.log(fruits)

/*
	sort()
		Rearranges the array elements in alphanumeric order

	Syntax:
		arrayName.sort();


*/

fruits.sort()
console.log("Mutated array from sort method");
console.log(fruits)


let mixedArray = [50 ,14, "Carlos", "Nej", "Bernard", "Charles"]
console.log(mixedArray.sort())


/*
	For sorting the items in descending order
	fruits.sort().reverse()
	console.log(fruits)

*/

/*
	reverse()
		Reverses the order of array elements

	Syntax:
		arrayName.reverse();


*/

fruits.reverse()
console.log("Mutated array from reverse method: ")
console.log(fruits)

/*
	Non-mutator methods
		These are functions that do not modify or change an array after they are created. These methods do not manipulate the original array performing various tasks such as returning element from an array and combining arrays and printing the output.

*/


let countries = ["US", "PH", "CA", "SG", "TH", "PH", "FR", "DE"];

/*
	indexOf()
		Returns the index number of the first matching element found in an array. If no match was found, the result will be -1. The search process will be done from first element proceeding to the last element

	Syntax:
		arrayName.indexOf(searchValue)
		arrayName.indexOf(searchValue, fromIndex)


*/

let firstIndex = countries.indexOf("PH");
console.log("Result of firstIndexOf method: " + firstIndex)


/*
	lastIndexOf()
		Returns the index number of the last matching element found in an array. The search process will be done from last element proceeding to the first element 

	Syntax:
		arrayName.lastIndexOf(searchValue)
		arrayName.lastIndexOf(searchValue, fromIndex)
*/

let lastIndex = countries.lastIndexOf("PH");
console.log("Result of lastIndexOf method: " + lastIndex)


let lastIndexStart = countries.lastIndexOf("PH", 4);
console.log("Result of lastIndexOf method: " + lastIndexStart)

/*
	slice()
		Portions/slice elements from an array AND returns a new array

	Syntax:
		arrayName.slice(startIndex)
		arrayName.slice(startingIndex, endingIndex)

*/

console.log(countries)
let slicedArrayA = countries.slice(2)
console.log("Result from slice method: ")
console.log(slicedArrayA)

let slicedArrayB = countries.slice(2, 4)
console.log("Result from slice method: ")
console.log(slicedArrayB)


let slicedArrayC = countries.slice(-3)
console.log("Result from slice method: ")
console.log(slicedArrayC)


/*
	toString()
		Returns an array as a string separated by commas

	Syntax:
		arrayName.toString()

*/

let stringArray = countries.toString()
console.log("Result from toString method: ")
console.log(stringArray)

/*
	concat()
		combines 2 arrays and returns the combined results
	Syntax:
		arrayA.concat(arrayB)
		arrayA.concat(elementA)
*/

let taskArrayA = ["drink html", "eat javascript",];
let taskArrayB = ["inhale css", "breath sass"]
let taskArrayC = ["get git", "be node"]

let tasks = taskArrayA.concat(taskArrayB)
console.log("Result from concat method:")
console.log(tasks)


//Combining Multiple Arrays
let allTasks = taskArrayA.concat(taskArrayB, taskArrayC);
console.log(allTasks)

//Combining arrays with elements
let combinedTasks = taskArrayA.concat("smell express", "throw react")
console.log(combinedTasks)


/*
	join()
		Returns an array as a string separator by specified separator string

	Syntax:
		arrayName.join(separatorString)

*/

let students = ["Tristan", "Bernard", "Carlos", "Nehemiah"];
console.log(students.join());
console.log(students.join(''));
console.log(students.join(" - "))



/*
	Iteration Methods are loops designed to perform repetitive tasks on arrays. Useful for manipulating array data resulting in complex tasks

*/

/*
	forEach()
		Similar to a for loop that iterated on each array element

	Syntax: 
		arrayName.forEach(function(individual element)) {
			statement
		}
*/


allTasks.forEach(function(task) {
	console.log(task)
})

//Using forEach with conditional statements
let filteredTasks = [];

allTasks.forEach(function(task) {

	if(task.length > 10) {
		filteredTasks.push(task)
	}
})

console.log("Result of filteredTasks: ");
console.log(filteredTasks)

/*
	map()
		Iterates on each element AND returns new array with different values depending on the result of the function's operation

	Syntax:
		let/cons resultArray = arrayName.map(function(indivdiualElement) {
	
			statement
		})

*/


let numbers = [1, 2, 3, 4, 5]

let numberMap = numbers.map(function(number) {

	console.log(number)
	return number * number
})

console.log("Original Array: ")
console.log(numbers)
console.log("Result of map method: ")
console.log(numberMap)


/*
	every()
		Checks if all elements in an array met the given condition
		Returns a true value if all elements meet condition and false if otherwise

	Syntax:
		let.const resultArray = arrayName.every(function(indivElement) {
			return expression/condition
		})
*/


let allValid = numbers.every(function(number) {
	return (number < 3);
})
console.log("Result of every method");
console.log(allValid)

/*
	some()
		Checks if at least one element in the array meets the given condition
		Returns a true value if atleats one element meets the given condition and false if otherwise


	Syntax:
		let.const resultArray = arrayName.some(function(indivElement) {
			return expression/condition
		})
	
*/


let someValid = numbers.some(function(number) {
		return(number < 2)
})

console.log("Result of some method");
console.log(someValid)

/*
	filter()
		Returns new array that contains elements which meets the given condition
		Return an empty array if no elements  were found

	Syntax:
		let.const resultArray = arrayName.filter(function(indivElement) {
				return expression/condition
			})
*/

let filterValid = numbers.filter(function(number) {
		return(number < 3)
})

console.log("Result of filter method");
console.log(filterValid)

let nothingFound = numbers.filter(function(number) {
		return(number == 0)
})

console.log("Result of filter method");
console.log(nothingFound)



// Filtering using forEach 

let filteredNumbers = [];

numbers.forEach(function(number){

	if(number <3) {
		filteredNumbers.push(number)
	}
})

console.log("Result of filtering using forEach method");
console.log(filteredNumbers)


let products = ["Mouse", "Keyboard", "Laptop", "Monitor", "Apple"];

/*
	includes()
		Methods can be "chained" using them one after another
		The result of the first method is used on the second method until all "chained" methods have been resolved
*/

let filteredProducts = products.filter(function(product){
		return product.toLowerCase().includes("a")
})

console.log(filteredProducts)


